package core.dao;


import core.entity.TicketOrder;

import java.io.IOException;
import java.util.List;

public interface TicketOrderDAO {

    void add(TicketOrder ticketOrder) throws IOException, ClassNotFoundException;
    List<TicketOrder> all() throws IOException, ClassNotFoundException;
    TicketOrder single(String name, String surname) throws IOException, ClassNotFoundException;

}
