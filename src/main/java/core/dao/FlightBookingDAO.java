package core.dao;


import core.entity.FlightBooking;
import core.errors.EntityNotFoundException;

import java.io.IOException;
import java.util.List;

public interface FlightBookingDAO {

    List<FlightBooking> all() throws IOException, ClassNotFoundException;
    FlightBooking single(int id) throws IOException, ClassNotFoundException;

    void insert(FlightBooking flightBooking) throws IOException, ClassNotFoundException;
    void update(FlightBooking flightBooking) throws EntityNotFoundException, IOException, ClassNotFoundException;
    void delete(FlightBooking flightBooking) throws IOException, ClassNotFoundException;




}
