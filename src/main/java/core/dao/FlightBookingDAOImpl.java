package core.dao;


import core.entity.FlightBooking;
import core.errors.EntityNotFoundException;
import core.util.PreparationDataBase;
import core.util.Templates;

import java.util.List;

public class FlightBookingDAOImpl implements FlightBookingDAO {
    @Override
    public List<FlightBooking> all() {
        Templates.data();
        return PreparationDataBase.readObject();
    }

    @Override
    public FlightBooking single(int id) {
        return Templates.data().stream().filter(e -> e.getId() == id).findFirst().get();
    }

    @Override
    public void insert(FlightBooking flightBooking) {
        Templates.data().add(flightBooking);
    }

    @Override
    public void update(FlightBooking flightBooking) throws EntityNotFoundException {
        FlightBooking emp = single(flightBooking.getId());
        if(emp == null)
            throw new EntityNotFoundException("Cannot find flightBooking for given criteria");

        emp.setName(flightBooking.getName());
    }

    @Override
    public void delete(FlightBooking flightBooking) {
        Templates.data().remove(flightBooking);
    }



    private int nextId() {
        return Templates.data().get(Templates.data().size() - 1).getId();
    }
}