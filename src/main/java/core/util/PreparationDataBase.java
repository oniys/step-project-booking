package core.util;

import core.entity.FlightBooking;
import core.entity.TicketOrder;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class PreparationDataBase {

    public void writeObject(List data) {
        try {
            FileOutputStream fos = new FileOutputStream(Properties.DATABASE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos);
            objectOutputStream.writeObject(data);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void writeOrder(List order) throws IOException {
        FileOutputStream fos = new FileOutputStream(Properties.DATABASE_ORDER);

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos);
            objectOutputStream.writeObject(order);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            fos.close();
        }
    }


    public static List<TicketOrder>  readOrder() {
        FileInputStream fis;
        List<TicketOrder> list = new ArrayList<TicketOrder>();
        try {
            fis = new FileInputStream(Properties.DATABASE_ORDER);
            ObjectInputStream objectInputStream = new ObjectInputStream(fis);
            list = (List<TicketOrder>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }


    public static List<FlightBooking> readObject() {
        FileInputStream fis;
        List<FlightBooking> list = new ArrayList<FlightBooking>();
        try {
            fis = new FileInputStream(Properties.DATABASE);

            ObjectInputStream objectInputStream = new ObjectInputStream(fis);
            list = (List<FlightBooking>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }
}



