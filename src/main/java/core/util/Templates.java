package core.util;

import core.entity.FlightBooking;
import core.util.enumTemplate.Flight;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Templates {
    public static List<FlightBooking> data() {
        List<FlightBooking> data = new ArrayList<>();
        Flight[] indata = Flight.values();
        for (Flight s : indata) {
            data.add(new FlightBooking(s.ordinal(), Properties._TRANSPORT,
                    indata[0].name(),
                    indata[(int) (Math.random() * (indata.length))].name(),
                    LocalDateTime.now().plusDays((int) (Math.random() * Properties._DAY))
                            .minusHours((int) (Math.random() * Properties._HOURS))
                            .minusMinutes((int) (Math.random() * Properties._MINUTES))
                            .minusSeconds((int) (Math.random() * Properties._SECONDS)),
                    (int) (Math.random() * (Properties._RAND_SEATS))
            ));
        }

        if (!Files.exists(Paths.get(Properties.DATABASE))) {
            System.out.println("SSSSSSSSSSSSSS");
            new PreparationDataBase().writeObject(data);
        }

        return PreparationDataBase.readObject();
    }

}




