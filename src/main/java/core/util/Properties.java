package core.util;

public class Properties {

    // Разброс во времени от текущего времени при создании "полетов" рандом!
    public static int _DAY = 3;
    public static int _HOURS = 24;
    public static int _MINUTES = 60;
    public static int _SECONDS = 60;
    //Вид транспорта
    public static String _TRANSPORT = "flight";
    // Рандомно генерируем свободные места
    public static int _RAND_SEATS = 68;

    //Название условной БАЗЫ-ДАННЫХ  ( можно обеденить в один фаил )
    public static String DATABASE = "DB.txt";
    public static String DATABASE_ORDER = "ORDER.txt";


    //TODO Клиент
    //Фильтрация списка отображения данных от текущего времени
    // по умолчанию +24 часа
    public static int _FUTURE_F = 24;




}
