package core.util;

import core.dao.TicketOrderDAO;
import core.dao.TicketOrderDAOImpl;
import core.entity.FlightBooking;
import core.entity.TicketOrder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TemplatesTicket {
    static List<TicketOrder> dataOrder = new ArrayList<>();
    private static TicketOrderDAO ticket_dao = new TicketOrderDAOImpl();
    private static boolean flag = true;

    public static void order(String name, String surname, FlightBooking ord) {
        dataOrder.add(new TicketOrder(name, surname, ord));
    }

    public static void save() throws IOException, ClassNotFoundException {
        if (!Files.exists(Paths.get(Properties.DATABASE_ORDER))) {
            new PreparationDataBase().writeOrder(dataOrder);
            flag = false;
        } else {
            if (flag) {
                List<TicketOrder> tiketns = ticket_dao.all();
                tiketns.stream().forEach(elem -> dataOrder.add(new TicketOrder(elem.getName(), elem.getSurname(), elem.getflight())));
                flag = false;
            }
            dataOrder.stream().forEach(elem -> System.out.println(
                    elem.getName() + " " +
                            elem.getSurname() + " | " +
                            elem.getflight().getStartOfFlight() + " ->-> " +
                            elem.getflight().getEndOfFlight()));
            new PreparationDataBase().writeOrder(dataOrder);
        }
    }



}