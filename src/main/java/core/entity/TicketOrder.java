package core.entity;

import java.io.Serializable;

public class TicketOrder implements Serializable {
    private String name;
    private String surname;
    private FlightBooking flight;


    public TicketOrder( String name, String surname, FlightBooking flight) {
        this.name = name;
        this.surname = surname;
        this.flight = flight;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public FlightBooking getflight() {
        return flight;
    }

    public void setflight(FlightBooking flight) {
        this.flight = flight;
    }
}
