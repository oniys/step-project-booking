package core.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class FlightBooking implements Serializable {
    private int id;
    private String name;
    private String startOfFlight;
    private String endOfFlight;
    private LocalDateTime date;
    private int seats;


    public FlightBooking(int id, String name, String startOfFlight, String endOfFlight, LocalDateTime date,int seats) {
        this.id = id;
        this.name = name;
        this.startOfFlight = startOfFlight;
        this.endOfFlight = endOfFlight;
        this.date = date;
        this.seats = seats;
    }

    public FlightBooking(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEndOfFlight() {
        return endOfFlight;
    }

    public String getStartOfFlight() {
        return startOfFlight;
    }

    public void setStartOfFlight(String startOfFlight) {
        this.startOfFlight = startOfFlight;
    }

    public void setEndOfFlight(String endOfFlight) { this.endOfFlight = endOfFlight; }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }


    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
}
