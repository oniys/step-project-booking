package client;


import core.dao.FlightBookingDAO;
import core.dao.FlightBookingDAOImpl;
import core.dao.TicketOrderDAO;
import core.dao.TicketOrderDAOImpl;
import core.entity.FlightBooking;
import core.util.Properties;
import core.util.TemplatesTicket;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static FlightBookingDAO dao = new FlightBookingDAOImpl();
    private static TicketOrderDAO ticket_dao = new TicketOrderDAOImpl();
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        loading(" Flight Booking :loading ", 10);
        comandRender();

        while (!scan.hasNext("[0-9]+")) {
            System.out.print("Wrong command: ");
            scan.next();
        }
        while (scan.hasNext()) {
            switch (scan.nextInt()) {
                case 1:
                    displayAllInfo();
                    end();
                    scan.nextInt();
                    break;
                case 2:
                    impromptuСleaning();
                    System.out.print("\nPleasse Enter id flight : ");
                    displayIdInfo(scan.nextInt());
                    end();
                    scan.nextInt();
                    break;
                case 3:
                    questionTrip();
                    end();
                    scan.nextInt();
                    break;
                case 4:
                    canceReservation();
                    break;
                case 5:
                    myInfo();
                    end();
                    scan.nextInt();
                    break;
                case 6:
                    System.exit(0);
            }
        }
    }


    private static void displayAllInfo() throws IOException, ClassNotFoundException {

        List<FlightBooking> flightBooking = dao.all();
        System.out.println("\nFlights in " + Properties._FUTURE_F + " hours:");
        flightBooking.stream().filter(
                p -> p.getDate().isAfter(LocalDateTime.now().plusHours(Properties._FUTURE_F))
        ).forEach(elem -> System.out.println(
                elem.getId() + " | "
                        + elem.getName() + " | "
                        + elem.getDate().toLocalTime().truncatedTo(ChronoUnit.SECONDS) + " | "
                        + elem.getDate().getDayOfMonth() + "-"
                        + elem.getDate().getMonthValue() + "-"
                        + elem.getDate().getYear() + " | "
                        + elem.getStartOfFlight() + " ---->>>>> "
                        + elem.getEndOfFlight()
        ));
    }

    private static void displayIdInfo(int id) throws IOException, ClassNotFoundException {
        FlightBooking flightBooking = dao.single(id);
        System.out.println("id: "
                + id + " | "
                + flightBooking.getName() + " free seats "
                + flightBooking.getSeats() + " | "
                + flightBooking.getStartOfFlight() + " -->--> "
                + flightBooking.getEndOfFlight() + " | "
                + flightBooking.getDate().toLocalTime().truncatedTo(ChronoUnit.SECONDS) + " | "
                + flightBooking.getDate().getDayOfMonth() + "-"
                + flightBooking.getDate().getMonthValue() + "-"
                + flightBooking.getDate().getYear()
        );
    }

    private static void loading(String inf, int mil) throws IOException, InterruptedException {
        String loa = "|/-\\";
        for (int x = 0; x < 101; x++) {
            String data = "\r" + loa.charAt(x % loa.length()) + inf + x;
            System.out.write(data.getBytes());
            Thread.sleep(mil);
        }
        System.out.println("\n");
    }


    private static void comandRender() {
        System.out.print("\n: 1 - online scoreboard ");
        System.out.print("\n: 2 - view flight information");
        System.out.print("\n: 3 - search and book a flight");
        System.out.print("\n: 4 - cancel the reservation");
        System.out.print("\n: 5 - my flights ");
        System.out.print("\n: 6 - Exit ");
        System.out.print("\nCommand: ");
    }

    private static void impromptuСleaning() {
        for (int i = 0; i < 50; ++i) System.out.println();
    }

    private static void end() {
        System.out.print("1 - Exit the menu: ");
        while (!scan.hasNext("[1]+")) {
            System.out.print("Wrong command: ");
            scan.next();
        }
        comandRender();
    }

    private static void questionTrip() throws IOException, ClassNotFoundException {
        System.out.println("\nSearch and book a flight: ");
        System.out.println("<Еnter destination, date, number of people>");
        System.out.print("<Please, Еnter destination> :");

        while (!scan.hasNext("[a-zA-Z]+")) {
            System.out.print("Incorrect the destination search parameters, please try again. :");
            scan.next();
        }
        String destination = scan.next();

        System.out.print("<Please, Еnter date '#-#-20##'> :");
        while (!scan.hasNext("[-0-9]+")) {
            System.out.print("Incorrect the date search parameters, please try again. :");
            scan.next();
        }
        String date = scan.next();

        System.out.print("<Please, Еnter number of people> :");
        while (!scan.hasNext("[0-9]+")) {
            System.out.print("Incorrect free places search parameters, please try again. :");
            scan.next();
        }
        int places = Integer.parseInt(scan.next());
        System.out.println("Search results for your query");
        List<FlightBooking> flightBooking = dao.all();
        flightBooking.stream().filter(
                p -> p.getEndOfFlight().equals(destination) &&
                        (p.getDate().getDayOfMonth() + "-"
                                + p.getDate().getMonthValue() + "-"
                                + p.getDate().getYear()).equals(date) &&
                        p.getSeats() >= places)
                .forEach(elem -> System.out.println(
                        elem.getId() + " | "
                                + elem.getName() + " free seats "
                                + elem.getSeats() + " | "
                                + elem.getDate().toLocalTime().truncatedTo(ChronoUnit.SECONDS) + " | "
                                + elem.getDate().getDayOfMonth() + "-"
                                + elem.getDate().getMonthValue() + "-"
                                + elem.getDate().getYear() + " | "
                                + elem.getStartOfFlight() + " ->->->->-> "
                                + elem.getEndOfFlight()

                ));
        System.out.println("\nTo order a ticket, enter its id or go to the main menu <end>: ");
        while (!scan.hasNext("[0-9-e-n-d]+")) {
            System.out.print("Incorrect parameters, please try again. :");
            scan.next();
        }
        String inp = scan.next();
        if (!inp.equals("end")) {
            FlightBooking ord = dao.single(Integer.parseInt(inp));
            System.out.println("Еnter the first and last name of the passenger: ");
            for (int i = 1; i <= places; i++) {
                System.out.println(i+") " + "Name <Enter> Surname <Enter>");
                TemplatesTicket.order(scan.next(),scan.next(),ord);
            }
            TemplatesTicket.save();
        }
    }


    public static void canceReservation(){

    }

    public static void myInfo() throws IOException, ClassNotFoundException {
        System.out.println("My flights search:");
        System.out.println("Name <Enter> Surname <Enter>");
        ticket_dao.single(scan.next(),scan.next());
    }
}
